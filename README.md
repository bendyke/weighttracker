<br><br>
<h1><b>Weight Tracker Projekt </b></h1>
<h2>Rövid összefoglalás és kérdések</h2>

___

<br><br>
### __Használt Toolchain__
<br>


- Typescript
- npm modulok
  - Chart.js
  - ts-loader
- Webpack


<br>
<h3><b>Indítás (valószínűleg triviális ...)</b></h3>
<br>

```JSON
npm install;
npm run build;
npm run start;
```

<br>
<h1>Input validálás</h1>
<br>

[weightEntryInput file](/src/weightEntryInput.ts) <br><br>
<p1>
- A validálást a submit gomb onClick eventjében kezeljük, csak minden feltétel teljesülésekor tároljuk az inputot. (> 0 tesztet nem használtam mert nem kérte az anyag, de kéne)

- Az egyszerűség kedvéért Alert()-el kérjük a felhasználót, hogy pontosítsa a hibás adatokat

<br>
<h1>Storage</h1>
<br>

[storage file](/src/storage.ts) <br><br>

<h3> localStorage és Cache</h3> <br>
<p1>
Mivel a localStorage csak key/value párokat tud tárolni amit automatikusan rendez, 
egy Cache-t használunk, amit 

```typescript
type cachedEntry = [Date, number];
cachedStorage: Array<cachedEntry>;
```
formában tárolunk.

Ezt a cache-t pedig egészében mentjük a localStorage-ba, mint:

```typescript
localStorage_db.setItem('storage', JSON.stringify(cache));
```
A rendezési és sub-cachek létrehozási folyamatokhoz csak a folyamatosan frissített
Cache-t használjuk.


<i>Megjegyzés: Mivel minden új entry-nél frissíteni kell a Cache-ünket, nagy adat volumennél
lassul ez a megodlás, oda egy IndexedDB jobb lenne, de itt a célnak megfelelően teljesít.</i>
</p1>

<br>
<h1>Statisztika</h1>
<br>

[statisticsController file](/src/statisticsContoller.ts) <br><br>

<h3>Statisztikai adatok</h3>

[statisticsDatahandler file](/src/statisticsDataHandler.ts) <br><br>

<p1>
Az adatokat a kiválasztott intervallumon mindig 

```typescript
dataFromInterval: Array<cachedEntry>;
```
formában kezeljük.

Az adatokat a [textFormatting fájlban](/src/textFormatting.ts) lévő metódusokkal 
formatáljuk és feedeljük a megfelelő HTMl komponensekbe.
</p1>
<br>
<h3>Chart</h3>

[statisticsChart file](/src/statisticsChart.ts) <br><br>

<p1>
A Chart.js lib-et felhasználva, a statisztika Controller fájlból feedelve az adatokat,
folyamatosan újrarajzoljuk a chart-ot. 
</p1>

<br><br><br>
<h1>Kérdések</h1>
<br>

<h4>

- A Chart-os rész a Git repo-ban és a videóban is zavaróan volt megfogalmazva
  , azt kéri az anyag, hogy az X-tengely címkéi "01 Jan" formában szerepeljenek.
    ha van egy "2021 Jun 10" és egy "2020 jun 16" rekordom egymás meleltt 
    (bármilyen okból is hagyott ki a felhasználó pont 1 évet), az feltűntetve
    elég bután néz ki. 

    Mindenesetre megoldottam, ahogyan kérte a feladat.
- Az src/ mappát szerettem volna még egy kicsit rendezni, főleg almappákat gyártani pl. a
  statisztika fájloknak, de amint almappába kerültek a fájlok, az importok 
  amik a mappán kívülre mutatnak semmilyen formában nem működnek. 

  <i>(De abszolut lehet, hogy csak én voltam balf*sz.)</i>


