
import Chart from 'chart.js/auto'


const getCanvasElementById = (id: string): HTMLCanvasElement => {
    const canvas = document.getElementById(id);

    if (!(canvas instanceof HTMLCanvasElement)) {
        throw new Error(`The element of id "${id}" is not a HTMLCanvasElement. Make sure a <canvas id="${id}""> element is present in the document.`);
    }

    return canvas;
}

const getCanvasRenderingContext2D = (canvas: HTMLCanvasElement): CanvasRenderingContext2D => {
    const context = canvas.getContext('2d');

    if (context === null) {
        throw new Error('This browser does not support 2-dimensional canvas rendering contexts.');
    }

    return context;
}

const ctx: CanvasRenderingContext2D = getCanvasRenderingContext2D(getCanvasElementById('chartCanvas'));

var testChart: Chart| undefined;
  
  
function drawChart(datasetToChart: number[], chartlabels: string[]): void {
    const labels: string[]= chartlabels;
    const dataSet: number[] = datasetToChart

    const data = {
        labels: labels,
        datasets: [{
            label: "",
            borderColor: 'rgb(36, 52, 132)',
            data: dataSet,
        }]
    };
    //clean canavas for new chart if already exists
    if (testChart as Chart) {
        testChart?.destroy();
    }

    testChart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: {
            scales: {
                y: {
                    ticks: {
                        // Include a dollar sign in the ticks
                        stepSize: 0.5,
                        callback: function(value, index, values) {
                            if (parseFloat(value.toString().valueOf()) % 1 == 0) {
                                return value + ".0 kg"
                            }
                            else return value + " kg"
                            
                        }
                    }
                }
            },
            plugins: {
                legend: {
                    display: false
                }
            }
        }
    });
}

export {drawChart as chartDrawer};
