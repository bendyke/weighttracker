import { validInput } from "./weightEntryInput";
import { populateHistoryList} from './weightHistoryList'

const localStorage_db: Storage = window.localStorage;

// Custom TYPE for a cached entry -- store an array of these as cache
export type cachedEntry = [Date, number];
var cachedStorage: Array<cachedEntry>;


/*  
    We cache everything from localStorage into an array of <cachedEntry>  
    AND store the cache Stringyfied to prevent localStorage from auto-sorting it

    Works fine but slows down at large data volumes -- probably should use indexedDB for that
*/

//cache local storage
function cacheLocalStorage(): void {

    //cache storage from localStorage
    cachedStorage = JSON.parse(localStorage_db.getItem('storage')!)
}


//store cache in localStorage
function storeCacheInlocalStorage(cache: Array<cachedEntry>): void{
    localStorage_db.setItem('storage', JSON.stringify(cache));
}


//Add into CACHE first and parse into LocalStorage
export function addItem(newMember: validInput): void {
    //init an empty cache if there was no previous data during initial init
    if (cachedStorage == null) {cachedStorage = [];}
    //we can use indexes here, since we know the tuple is max 2 members long
        //1 is Date, 0 is Number -- maybe needs reformat?
    cachedStorage.unshift([newMember[1], newMember[0]])
    //re-store cache --> bad at large data volumes!
    storeCacheInlocalStorage(cachedStorage);
    //re-cache
    cacheLocalStorage();
    //re-populate history list 
    populateHistoryList(cachedStorage);
}


export function initCacheData(): void{
    cacheLocalStorage();
    if (cachedStorage !== null) {
        populateHistoryList(cachedStorage);
    }
};


export function getCache(): Array<cachedEntry> | null{
    if (cachedStorage !== undefined) {
        return cachedStorage;
    }
    return null;
}


//sort cache according to date
export function getSortedCache(cache: Array<cachedEntry>):Array<cachedEntry>  {
    let sortedCache: Array<cachedEntry>;

    sortedCache = cache.sort(function(a, b) { 
        let aDate: Date = new Date(a[0]);
        let bDate: Date = new Date(b[0]);
        return +aDate - +bDate;
    });
    return sortedCache;
}

