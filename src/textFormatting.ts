/*
class storing formatting functions
*/

const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];


export function formatFixTen(n: number): string{
    if(n < 10) {
        return "0" + n.toString();
    }
    else{
        return n.toString();
    }
}

export function formatFixDecimal(s: string, n: number): string {
    if(n % 1 === 0 || n % -1 === 0){
        return s + ".0";
    }
    else{

        return s;
    }
}


export function formatCachedDateEntry(index: number, cacheEntry: Date): string {
    let dateString: string = "";
    //date formatting
    let dateData: Date = new Date(cacheEntry);
    let minutes: number = dateData.getMinutes();
    let hours: number  = dateData.getHours();
    let day: number = dateData.getDate();
    let month: number = dateData.getMonth();
    let year: number = dateData.getFullYear();
    let current: Date = new Date();
    let yesterday: Date = new Date();
    yesterday.setDate(current.getDate()-1);

    //date is today
    if (current.getDate() == day && current.getMonth() == month) {
        let minutesFixed: string = minutes.toString();
        let hoursFixed: string = hours.toString();
        minutesFixed = formatFixTen(minutes);
        hoursFixed = formatFixTen(hours)
        dateString = 'today at ' + hoursFixed + ":" + minutesFixed;

        return dateString;
    }
    //date is yesterday
    if (yesterday.getDate() == day) {
        let minutesFixed: string = minutes.toString();
        let hoursFixed: string = hours.toString();
        minutesFixed = formatFixTen(minutes);
        hoursFixed = formatFixTen(hours)
        dateString = 'yesterday at ' + hoursFixed + ":" + minutesFixed;
        return dateString;
    }

    //date is this year
    if(current.getFullYear() == year && dateString == "") {
        //console.log(dateString);
        dateString = formatFixTen(day) 
        + " " 
        + monthNames[month] 
        + " at "
        + formatFixTen(hours) 
        + ":"
        + formatFixTen(minutes);

        return dateString;
    }
    //other date
    else{
        dateString = formatFixTen(day) 
        + " " 
        + monthNames[month] 
        + " " 
        + year
        + " at "
        + formatFixTen(hours) 
        + ":"
        + formatFixTen(minutes);

        return dateString;
    }
}

export function formatCachedWeightEntry(index: number, cache: number): string {
     //weight formatting
    let weightString: string;
    let weightFixed: string;
    let weightData: number = cache; 
    weightFixed = weightData.toString();
    weightFixed = formatFixTen(weightData);
    weightFixed = formatFixDecimal(weightFixed, weightData);
    weightString = weightFixed + " kg";
    return weightString;
}

export function formatDateForChart(s: string, d: Date): string{
    d = new Date(d);
    s = formatFixTen(d.getDate()) + ' ' + monthNames[d.getMonth()];
    return s;
}