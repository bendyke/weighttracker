/*  
    App Entry Point
*/

import { initUserInput } from "./weightEntryInput";
import { initStatisticsData } from "./statisticsContoller";
import { initCacheData } from "./storage";



//localStorage.clear();

initUserInput();
initCacheData();
initStatisticsData();
