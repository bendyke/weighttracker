/* 
    Input validation
    Only store inputs which pass all required checks, 
    use Alert() to notify user about incorerct inputs
*/



import { addItem } from "./storage";

const dateInput: HTMLInputElement = <HTMLInputElement> document.getElementById('weightDate');
const weightInput: HTMLInputElement = <HTMLInputElement> document.getElementById('weightNumber');
const submitButton = document.getElementById('weightSubmitButton');

//define a cusotm tuple type for valid input memebrs
export type validInput = [number, Date];
var validatedInput: validInput;


//init Btn EventListener
export function initUserInput(): void {

    if (dateInput && weightInput) {
        submitButton?.addEventListener('click', (e: Event) => {
            
            if(validateInput(dateInput, weightInput)) {
                if (validatedInput) {
                    addItem(validatedInput);
                }
            }

        });
    }
}

//validate input and return true if validated and update validatedInput Var
function validateInput(dateInputElement: HTMLInputElement, weightInputElement:HTMLInputElement):boolean {
    let dateInputDate: Date = new Date(dateInputElement.value);
    let currentDate: Date = new Date();

    //round weight value to 1 decimal
    let weightInput: number = weightInputElement.valueAsNumber;

    //date cannot be a future time!
    if (dateInputDate < currentDate) {
        //check weight input
        if (weightInputElement.valueAsNumber) {
            if (weightInput % 1 == 0) {
                validatedInput = [weightInput, dateInputDate];
                return true;
            }
            else{
                if (weightInput.toString() == weightInput.toFixed(1)) {
                    validatedInput = [weightInput, dateInputDate];
                return true;
                }
                else{ alert("Please use maximum 1 decimal number for your weight!"); return false;}
            }          
        }
        else{
            alert("Required field is missing! Please fill out your weight!");
            return false;
        }
    }
    else{
        alert("Future dates are not allowed! Please check your input...");
        return false;
    }
}