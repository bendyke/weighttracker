import { DatasetController } from "chart.js";
import { getCache, cachedEntry } from "./storage";

var lifetimeData: Array<cachedEntry>| null = getCache();


export function selectWeekData(): Array<cachedEntry> {
    //create dataset wtih last weeks entries
    //Only LAST entry for each day!
    let weeklyData: Array<cachedEntry> = [];
    lifetimeData = getCache();
    if (lifetimeData !== null) {
        let today: Date = new Date();
        let thisWeek: Date = new Date(today.getFullYear(), today.getMonth() ,today.getDate() - 7)
        let i:number = 0;
        let elementDate: Date;
        let elementBeforeDate: Date;
        for(i; i < lifetimeData.length; i++){ 
            if (i !== 0) {
                elementBeforeDate = new Date(lifetimeData[i-1][0]);
            }
            else{
                elementBeforeDate = new Date(lifetimeData[i][0]);
            }

            elementDate = new Date(lifetimeData[i][0]);
            elementDate = new Date(elementDate.getFullYear() ,elementDate.getMonth(), elementDate.getDate())
            if (elementDate > thisWeek && elementBeforeDate.getDate() !== elementDate.getDate()) {
                weeklyData.unshift(lifetimeData[i]);
            }
        }
    }
    return weeklyData;
}

export function selectMonthData(): Array<cachedEntry> {
    //create dataset wtih last months entries
    //Only LAST entry for each day!
    let monthlyData: Array<cachedEntry> = [];
    lifetimeData = getCache();
    if (lifetimeData !== null) {
        let today: Date = new Date();
        let thisMonth: Date = new Date(today.getFullYear(), today.getMonth() -1  ,today.getDate())
        let i:number = 0;
        let elementDate: Date;
        let elementBeforeDate: Date;
        for(i; i < lifetimeData.length; i++){ 
            if (i !== 0) {
                elementBeforeDate = new Date(lifetimeData[i-1][0]);
            }
            else{
                elementBeforeDate = new Date(lifetimeData[i][0]);
            }
            elementDate = new Date(lifetimeData[i][0])
            elementDate = new Date(elementDate.getFullYear() ,elementDate.getMonth(), elementDate.getDate())

            if (elementDate > thisMonth && elementBeforeDate.getDate() !== elementDate.getDate()) {
                monthlyData.unshift(lifetimeData[i]);
            }
        }
    }
    return monthlyData;
}

export function selectYearData(): Array<cachedEntry> {
    //create dataset wtih last years entries
    //Only LAST entry for each day!
    let yearlyData: Array<cachedEntry> = [];
    lifetimeData = getCache();
    if (lifetimeData !== null) {
        let today: Date = new Date();
        let thisYear: Date = new Date(today.getFullYear()-1, today.getMonth()  ,today.getDate())
        let i:number = 0;
        let elementDate: Date;

        let elementBeforeDate: Date;
        for(i; i < lifetimeData.length; i++){ 
            if (i !== 0) {
                elementBeforeDate = new Date(lifetimeData[i-1][0]);
            }
            else{
                elementBeforeDate = new Date(lifetimeData[i][0]);
            }
            elementDate = new Date(lifetimeData[i][0])
            elementDate = new Date(elementDate.getFullYear() ,elementDate.getMonth(), elementDate.getDate())
            if (elementDate > thisYear && elementBeforeDate.getDate() !== elementDate.getDate()) {
                yearlyData.unshift(lifetimeData[i]);
            }
        }
    }
    return yearlyData;
}

export function selectLifetimeData(): Array<cachedEntry> {
    //Only LAST entry for each day!
    let lifetimeData: Array<cachedEntry> = [];
    let sortedLifetimeData: Array<cachedEntry> = [];
    if (getCache() !== null) {
        lifetimeData = getCache()!;
        let i:number = 0;
        let elementDate: Date;
        let elementBeforeDate: Date;
        for(i; i < lifetimeData.length; i++){ 
            if (i !== 0) {
                elementBeforeDate = new Date(lifetimeData[i-1][0]);
            }
            else{
                elementBeforeDate = new Date(lifetimeData[i][0]);
            }
            elementDate = new Date(lifetimeData[i][0])
            elementDate = new Date(elementDate.getFullYear() ,elementDate.getMonth(), elementDate.getDate())
            if (elementBeforeDate.getDate() !== elementDate.getDate()) {
                sortedLifetimeData.unshift(lifetimeData[i]);
            }
        }
    }
    
    return sortedLifetimeData;
}

