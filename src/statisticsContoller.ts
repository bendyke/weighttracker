import { getCache, getSortedCache, cachedEntry } from './storage';
import * as statsDataHandler from './statisticsDataHandler'
import { formatFixDecimal, formatDateForChart } from "./textFormatting";
import { chartDrawer } from './statisticsChart';


const selectWeekButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById('weekButton');
const selectMonthButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById('monthButton');
const selectYearButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById('yearButton');
const selectLifetimeButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById('lifetimeButton');
var buttons: Array<HTMLButtonElement> = [];


const currentWeightText: HTMLElement = document.getElementById('currentWeightText')!
const periodStartWeightText: HTMLElement = document.getElementById('periodStartWeightText')!
const weightProgressText: HTMLElement = document.getElementById('weightProgressText')!


var cacheForSorting: Array<cachedEntry>;

export function initStatisticsData(): void {

    selectWeekButton.addEventListener('click', (e: Event) => {
        updateStatisticsElements(statsDataHandler.selectWeekData);
        activateButton(selectWeekButton);
    });
    selectMonthButton.addEventListener('click', (e: Event) => {
        updateStatisticsElements(statsDataHandler.selectMonthData);
        activateButton(selectMonthButton);
    });
    selectYearButton.addEventListener('click', (e: Event) => {
        updateStatisticsElements(statsDataHandler.selectYearData);
        activateButton(selectYearButton);
    });
    selectLifetimeButton.addEventListener('click', (e: Event) => {
        updateStatisticsElements(statsDataHandler.selectLifetimeData);
        activateButton(selectLifetimeButton);
    });

    buttons.unshift(selectWeekButton, selectMonthButton, selectYearButton, selectLifetimeButton);

    //autoslect week!
    updateStatisticsElements(statsDataHandler.selectWeekData);
    activateButton(selectWeekButton);
}



//update statistics elements on page
function updateStatisticsElements(getRelevantData: Function): void {
    //get and sort the selectedData and full caches
    if (getCache()) {
        cacheForSorting = getSortedCache(getCache()!);
    }
    let selectedData: Array<cachedEntry> =  getRelevantData();
    selectedData = getSortedCache(selectedData);
    prepareDataForUpdate(selectedData, cacheForSorting);

}


//update statistics number elements
function prepareDataForUpdate(selectedDataCache: Array<cachedEntry>, fullCache: Array<cachedEntry>){
    let currentWeight: number;
    let idx: number;
    let periodStartElement: cachedEntry;
    let periodStartWeight: number

    if (fullCache !== undefined) {
        if (fullCache.length >= 1) {

            //prepare and update text elements
            currentWeight = fullCache[fullCache.length-1][1];
            idx = fullCache.findIndex(e => e == selectedDataCache[0]);
            if (idx > 0) {
                periodStartElement = fullCache[idx-1];
                periodStartWeight = fullCache[idx-1][1];
            }
            else{
                periodStartElement = fullCache[idx];
                periodStartWeight = fullCache[idx][1]; 
            }   
             
            updateTextElements(currentWeight, periodStartWeight);

            //prepare and update Chart
            let chartDataCache: Array<cachedEntry> = [];
            selectedDataCache.forEach(element => {chartDataCache.push(element);});
            chartDataCache.unshift(periodStartElement);


            updateChartElement(chartDataCache);
        }
     }


} 

function updateTextElements(currentWeight: number, periodStartWeight: number): void{
    currentWeightText.innerHTML = formatFixDecimal(currentWeight.toString(), currentWeight) + " kg";
    periodStartWeightText.innerHTML = formatFixDecimal(periodStartWeight.toString(), periodStartWeight) + " kg";
    let progress: number = currentWeight - periodStartWeight;
    let progS: string = progress.toFixed(1);
    weightProgressText.innerHTML = formatFixDecimal(progS + " kg", progress);

}


function updateChartElement(chartDataCache: Array<cachedEntry>) {    
    
    let labels: string[] = [];
    let data: number[] = [];
    let tempS: string;

    chartDataCache.forEach(element => {
        tempS = formatDateForChart(tempS, element[0]);
        labels.push(tempS);
        data.push(element[1]);
    });

    //export to chartDrawer and Draw chart
    chartDrawer(data, labels);
}





//mark selection button as active and deactivate other buttons
function activateButton(btn: HTMLButtonElement): void {
    let buttonToActivate: HTMLButtonElement| undefined = buttons.find(element => element == btn);

    if (buttonToActivate !== undefined) {
        buttonToActivate.style.backgroundColor="rgb(53, 52, 52)";
        buttonToActivate.style.borderBlockColor="rgb(0, 124, 155)";
    }

    deactivateButtons(btn);
}

function deactivateButtons(btn: HTMLButtonElement): void {

    for (let i = 0; i< buttons.length; i++) {

        if (buttons[i] != btn && buttons[i] !== undefined ) {
            buttons[i].style.backgroundColor="rgb(88, 88, 88)";
            buttons[i].style.borderBlockColor="rgb(88, 88, 88)";
        }
    }
    
}