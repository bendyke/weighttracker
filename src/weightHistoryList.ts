import { cachedEntry } from "./storage";
import { formatCachedDateEntry, formatCachedWeightEntry } from "./textFormatting";

const historyList: HTMLElement = document.getElementById('weightHistoryList')!;    



export function populateHistoryList(cache: Array<cachedEntry>) {
    //clear list
clearHistoryList();

//only the first 10
for(let i =0; i < 10; i++){
    if (cache[i]) {
        //create span and li child element
        var li: HTMLLIElement = document.createElement("li");
        var weightSpan: HTMLSpanElement = document.createElement("span");
        var dateSpan: HTMLSpanElement = document.createElement("span");
        //process from cache
        weightSpan.innerHTML = formatCachedWeightEntry(i, cache[i][1]);
        li.appendChild(weightSpan).className = "weightListEntry_Weight";

        dateSpan.innerHTML = formatCachedDateEntry(i, cache[i][0]);
        li.appendChild(dateSpan).className = "weightListEntry_Date";

        historyList?.appendChild(li);
    }else{break;}
    }
}
    


function clearHistoryList(): void {
    let childsL: number = historyList.childNodes.length;

    for(var i=0; i < childsL; i++){
        historyList.removeChild(historyList.firstChild!);
    }
}