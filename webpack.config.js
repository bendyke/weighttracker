const path = require('path');

module.exports = {
    mode: "none",
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
    },
    entry: './src/app.ts',
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                include: [path.resolve(__dirname, 'src')]
            }
        ]
    },
output: {
    filename: 'bundled.js',
    path: path.resolve(__dirname, './dist'),
    },
devServer: {
    static: "./dist",
    hot: true,
},

}